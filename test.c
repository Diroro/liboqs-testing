#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <oqs/oqs.h>    //Librería vinculada

#define SEED_BYTES 48

//Función para mostrar las claves y los mensajes en ASCII
void print_ascii(const uint8_t* s, size_t len, FILE* f){
    size_t i;
	for (i = 0; i < len; i++) {
		fprintf(f, "%02X", s[i]);
	}
	if (len == 0) {
		fprintf(f, "00");
	}
	fprintf(f, "\n");
}

void test_kem(const char* kem_names[], const int kem_n, FILE* f){
    OQS_KEM* kem = NULL;
    uint8_t *public_key = NULL;
    uint8_t *secret_key = NULL;
    uint8_t *ciphertext = NULL;
    uint8_t *shared_secret_e = NULL;
    uint8_t *shared_secret_d = NULL;
    int i;
    //Semillas
    
    //Test
    for(i = 0; i < kem_n; i++){
        kem = OQS_KEM_new(kem_names[i]);
        if (kem == NULL) {
            printf("KEM %s not found in LIBOQS", kem_names[i]);
        }
        else{
			//Asigna memoria dinamica
            public_key = malloc(kem->length_public_key);
	        secret_key = malloc(kem->length_secret_key);
	        ciphertext = malloc(kem->length_ciphertext);
	        shared_secret_e = malloc(kem->length_shared_secret);
	        shared_secret_d = malloc(kem->length_shared_secret);
			//KEM
            fprintf(f, "KEM loaded: %s\n", kem_names[i]);
            OQS_KEM_keypair(kem, public_key, secret_key);
            fprintf(f, "Public key:");
            print_ascii(public_key, kem->length_public_key, f);
            fprintf(f, "Secret key:");
            print_ascii(secret_key, kem->length_secret_key, f);
            OQS_KEM_encaps(kem, ciphertext, shared_secret_e, public_key);
            fprintf(f, "Ciphertext for:");
            print_ascii(ciphertext, kem->length_ciphertext, f);
            fprintf(f, "Shared secret sent:\n");
            print_ascii(shared_secret_e, kem->length_shared_secret, f);
            OQS_KEM_decaps(kem, shared_secret_d, ciphertext, secret_key);
            fprintf(f, "Shared secret received:");
            print_ascii(shared_secret_d, kem->length_shared_secret, f);
			//Libera memoria dinamica
            OQS_MEM_secure_free(secret_key, kem->length_secret_key);
		    OQS_MEM_secure_free(shared_secret_e, kem->length_shared_secret);
		    OQS_MEM_secure_free(shared_secret_d, kem->length_shared_secret);
	        OQS_MEM_insecure_free(public_key);
	        OQS_MEM_insecure_free(ciphertext);
	        OQS_KEM_free(kem);
        }
		
    }
}


void test_sig(const char* sign_names[], const int sign_n, FILE* f){
    OQS_SIG *sig = NULL;
	uint8_t *public_key = NULL;
	uint8_t *secret_key = NULL;
	uint8_t *message = NULL;
	size_t message_len = 100;
	uint8_t *signature = NULL;
	size_t signature_len;
    int i;

    for(i = 0; i < sign_n; i++){
		sig = OQS_SIG_new(sign_names[i]);
		if(sig ==NULL){
			 printf("Signature scheme %s not found in LIBOQS", sign_names[i]);
		}
		else{
			//Asigna memoria dinamica
			public_key = malloc(sig->length_public_key);
	        secret_key = malloc(sig->length_secret_key);
	        message = malloc(message_len);
	        signature = malloc(sig->length_signature);
            signature_len = sig->length_signature;
			//Firma
            fprintf(f, "Signature scheme loaded: %s\n", sign_names[i]);
            OQS_SIG_keypair(sig, public_key, secret_key);
            fprintf(f, "Public key:");
			print_ascii(public_key, sig->length_public_key, f);
            fprintf(f, "Secret key:");
			print_ascii(secret_key, sig->length_secret_key, f);
            fprintf(f, "Message:");
            OQS_SIG_sign(sig, signature, &signature_len, message, message_len, secret_key);
            fprintf(f, "Signed message:");
			print_ascii(signature, signature_len, f);
			//Verifica firma
            OQS_STATUS s = OQS_SIG_verify(sig, message, message_len, signature, signature_len, public_key);
            if(s != OQS_SUCCESS){
                fprintf(f, "Signed message couldnt be verified\n");
            }
            else{
                fprintf(f, "Signed message verified\n");
            }
            //Modificación de la firma para invalidar
            OQS_randombytes(signature, signature_len);
            fprintf(f, "Modifying message to: %s\n", signature);
            s = OQS_SIG_verify(sig, message, message_len, signature, signature_len, public_key);
            if(s != OQS_SUCCESS){
                fprintf(f, "Modified message couldnt be verified\n");
            }
            else{
                fprintf(f, "Modified message verified\n");
            }
			//Libera memoria dinamica
			OQS_MEM_secure_free(secret_key, sig->length_secret_key);
			OQS_MEM_secure_free(message, message_len);
			OQS_MEM_insecure_free(public_key);
			OQS_MEM_insecure_free(signature);
			OQS_SIG_free(sig);
        }
    }
}

int main(int argc, char **argv) {
    OQS_init();
    FILE* f = stdout;
	uint8_t seed[SEED_BYTES];
	if(argc == 1){
		OQS_randombytes(seed, SEED_BYTES);
	}
    if(argc == 2 ){   //se puede especificar un fichero en el que volcar los datos
        f = fopen(argv[1], "w+");
		OQS_randombytes(seed, SEED_BYTES);
    }
	else if (argc == 3){	//se puede añadir la semilla con la que usar los hashes
		f = fopen(argv[1], "w+");
		memcpy(seed, argv[2], SEED_BYTES);
	}
	else{
		printf("Wrong format, try: %s <exit file path> <seed>", argv[0]);
	}
	fprintf(f, "Seed=");
	print_ascii(seed, SEED_BYTES, f);
	//Aquí se especifican los algoritmos a probar
    const int kem_n = 9;
    const int sign_n = 3;
    const char* kem_names[9] = {"Kyber512", "Kyber768", "Kyber1024", "FrodoKEM-640-AES", "FrodoKEM-976-AES", "FrodoKEM-1344-AES", "FrodoKEM-640-SHAKE", "FrodoKEM-976-SHAKE", "FrodoKEM-1344-SHAKE"};
    const char* sign_names[3] = {"Dilithium2", "Dilithium3", "Dilithium5"};
    test_kem(kem_names, kem_n, f);
    test_sig(sign_names, sign_n, f);
    return 0;
}

